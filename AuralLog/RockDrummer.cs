﻿using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class RockDrummer : IHandle<MetronomeTickEvent>
    {
        private readonly IPublishToTopics bus;

        public RockDrummer(IPublishToTopics bus)
        {
            this.bus = bus;
        }

        public void Handle(MetronomeTickEvent message)
        {
            if (message.TicksPerBeat % 2 != 0)
                return; // what to do?! RockDrummer hates odd time signatures!

            if (message.BeatTick % 2 == 0)
            {
                bus.Publish(new PercussionOutputEvent
                {
                    Hits = new[] { Percussion.BassDrum1, Percussion.SnareDrum1, Percussion.SplashCymbal },
                    Velocity = 100,
                    TicksPerBeat = 4
                });
                return;
            }
            else
            {
                bus.Publish(new PercussionOutputEvent
                {
                    Hits = new[] { Percussion.BassDrum2 },
                    Velocity = 100,
                    TicksPerBeat = 4
                });
                return;
            }
        }
    }
}
