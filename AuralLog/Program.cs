﻿using System.Threading;
using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class Program : ProgramBase
    {
        protected override void Start()
        {
            // Set instruments etc from config here
            var outputDevice = OutputDevice.InstalledDevices[0];
            outputDevice.Open();
            outputDevice.SendProgramChange(Channel.Channel1, Instrument.AcousticGrandPiano);
            outputDevice.SendProgramChange(Channel.Channel2, Instrument.AltoSax);

            var beatsPerMinute = 60;

            var bus = new TopicBasedPubSub();

            var quantizer = new Quantizer(outputDevice, beatsPerMinute);
            var quantizerNoteThread = quantizer.WithThreadedQueue<NoteOutputEvent>();
            var quantizerPercussionThread = quantizer.WithThreadedQueue<PercussionOutputEvent>();
            bus.Subscribe(quantizerNoteThread);
            bus.Subscribe(quantizerPercussionThread);

            var inputIntensityRater = new InputIntensityRater(bus);
            bus.Subscribe<MetronomeTickEvent>(inputIntensityRater);
            bus.Subscribe<InputReceivedEvent>(inputIntensityRater);

            var jazzDrummer = new JazzDrummer(bus);
            bus.Subscribe<MetronomeTickEvent>(jazzDrummer);
            bus.Subscribe<InputIntensityRatedEvent>(jazzDrummer);

            var jazzPianist = new JazzPianist(bus);
            bus.Subscribe(jazzPianist);

            var kbInput = new InputFromKeyboard(bus, Channel.Channel1);
            var fInput = new InputFromFile(bus, Channel.Channel2, @"c:\etc\temp\fakeLogs", "*.txt");
            var metronome = new Metronome(bus, beatsPerMinute, 8);

            var runnable = new CompositeRunnable(quantizerNoteThread, quantizerPercussionThread, metronome, kbInput, fInput);
            runnable.Start();

            Application.RegisterExitAction(i =>
            {
                runnable.Stop();
                outputDevice.Close();
            });

            while (true)
                Thread.Sleep(250);
        }

        static int Main(string[] args)
        {
            var p = new Program();
            return p.Run(args);
        }
    }
}
