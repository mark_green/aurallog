﻿using System;
using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class InputFromKeyboard : ThreadedRunnable
    {
        private readonly IPublishToTopics bus;
        private readonly Channel channel;
        public InputFromKeyboard(IPublishToTopics bus, Channel channel)
        {
            this.bus = bus;
            this.channel = channel;
        }

        protected override void WhileRunning()
        {
            var key = Console.ReadKey();

            if (key.Key == ConsoleKey.Escape)
            {
                Application.Exit(ExitCode.Success, "User requested quiting.");
            }

            bus.Publish(new InputReceivedEvent
            {
                Channel = channel
            });
        }
    }
}
