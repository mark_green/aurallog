﻿using System;
using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class Quantizer :
        IHandle<NoteOutputEvent>,
        IHandle<PercussionOutputEvent>,
        IDisposable
    {
        private readonly Clock clock;
        private readonly OutputDevice device;
        public Quantizer(OutputDevice device, int beatsPerMinute)
        {
            this.clock = new Clock(beatsPerMinute);
            this.device = device;

            clock.Start();
        }

        public void Handle(NoteOutputEvent message)
        {
            float nextTick = (float)Math.Ceiling(message.TicksPerBeat * clock.Time) / message.TicksPerBeat;

            foreach (var pitch in message.Pitches)
            {
                clock.Schedule(new NoteOnMessage(device, message.Channel, pitch, message.Velocity, nextTick));
                clock.Schedule(new NoteOffMessage(device, message.Channel, pitch, message.Velocity, nextTick + message.DurationInBeats));
            }
        }

        public void Handle(PercussionOutputEvent message)
        {
            float nextTick = (float)Math.Ceiling(message.TicksPerBeat * clock.Time) / message.TicksPerBeat;

            foreach (var percussion in message.Hits)
                clock.Schedule(new PercussionMessage(device, percussion, message.Velocity, nextTick));
        }

        public void Dispose()
        {
            clock.Stop();
        }
    }
}
