﻿using System;
using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class JazzPianist : IHandle<InputReceivedEvent>
    {
        private int currentOctave;
        private Random rand;
        private Func<int, Pitch[]>[] jazzChords;

        private readonly IPublishToTopics bus;
        public JazzPianist(IPublishToTopics bus)
        {
            this.bus = bus;

            this.currentOctave = 0;
            this.rand = new Random();
            this.jazzChords = new Func<int, Pitch[]>[] {
                octave => new[] { (Pitch)(24 + octave * 12 + 0), (Pitch)(24 + octave * 12 + 5), (Pitch)(24 + octave * 12 + 7) },
                octave => new[] { (Pitch)(24 + octave * 12 + 2), (Pitch)(24 + octave * 12 + 5), (Pitch)(24 + octave * 12 + 9), (Pitch)(24 + octave * 12 + 11) }
            };
        }

        public void Handle(InputReceivedEvent message)
        {
            bus.Publish(new NoteOutputEvent
            {
                Pitches = jazzChords[rand.Next(jazzChords.Length)](currentOctave),
                Channel = message.Channel,
                Velocity = 80,
                DurationInBeats = 1,
                TicksPerBeat = 8
            });

            currentOctave = (currentOctave + 1) % 6;
        }
    }
}
