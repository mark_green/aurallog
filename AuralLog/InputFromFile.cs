﻿using System.IO;
using System.Threading;
using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class InputFromFile : IAmRunnable
    {
        private volatile bool go;
        private Thread thread;

        private readonly IPublishToTopics bus;
        private readonly Channel channel;
        private readonly string path;
        private readonly string filter;
        public InputFromFile(IPublishToTopics bus, Channel channel, string path, string filter)
        {
            this.bus = bus;
            this.channel = channel;
            this.path = path;
            this.filter = filter;
        }

        void Run()
        {
            // Create a new FileSystemWatcher and set its properties.
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;

            // This determines what file attributes will trigger notifications
            watcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Filter = filter ;

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);

            // Begin watching.
            watcher.EnableRaisingEvents = true;

            while (go) Thread.Sleep(250);
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            bus.Publish(new InputReceivedEvent
            {
                Channel = channel
            });
        }

        public void Start()
        {
            thread = new Thread(() => Run());
            go = true;
            thread.Start();
        }

        public void Stop()
        {
            go = false;
            thread.Join();
        }
    }
}
