﻿using AuralLog.Infrastructure;

namespace AuralLog.Messages
{
    class InputReceivedEvent : Message
    {
        public Midi.Channel Channel { get; set; }
    }
}
