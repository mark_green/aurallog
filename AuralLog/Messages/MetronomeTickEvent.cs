﻿using AuralLog.Infrastructure;

namespace AuralLog.Messages
{
    class MetronomeTickEvent : Message
    {
        public int BeatNumber { get; set; }
        public int BeatTick { get; set; }

        public int TicksPerBeat { get; set; }
    }
}
