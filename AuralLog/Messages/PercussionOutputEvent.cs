﻿using AuralLog.Infrastructure;

namespace AuralLog.Messages
{
    class PercussionOutputEvent : Message
    {
        public Midi.Percussion[] Hits { get; set; }
        public int TicksPerBeat { get; set; }
        public int Velocity { get; set; }
    }
}
