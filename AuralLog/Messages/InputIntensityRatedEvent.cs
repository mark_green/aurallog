﻿using AuralLog.Infrastructure;

namespace AuralLog
{
    public class InputIntensityRatedEvent : Message
    {
        public float InputsPerMetronomeTick { get; set; }
    }
}
