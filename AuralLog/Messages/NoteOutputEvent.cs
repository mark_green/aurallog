﻿using AuralLog.Infrastructure;

namespace AuralLog.Messages
{
    class NoteOutputEvent : Message
    {
        public Midi.Pitch[] Pitches { get; set; }
        public Midi.Channel Channel { get; set; }
        public int TicksPerBeat { get; set; }
        public float DurationInBeats { get; set; }
        public int Velocity { get; set; }
    }
}
