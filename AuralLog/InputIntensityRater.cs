﻿using System;
using System.Threading;
using AuralLog.Infrastructure;
using AuralLog.Messages;

namespace AuralLog
{
    class InputIntensityRater : IHandle<MetronomeTickEvent>, IHandle<InputReceivedEvent>
    {
        private int inputsThisInterval;
        private readonly object inputsThisIntervalLock = new object();
        private readonly IPublishToTopics bus;
        public InputIntensityRater(IPublishToTopics bus)
        {
            this.bus = bus;
        }
    
        public void Handle(MetronomeTickEvent message)
        {
            var ratedEvent = new InputIntensityRatedEvent();
            lock(inputsThisIntervalLock)
            {
                ratedEvent.InputsPerMetronomeTick = inputsThisInterval;
                inputsThisInterval = 0;
            }
            Console.WriteLine(ratedEvent.InputsPerMetronomeTick);
            bus.Publish(ratedEvent);
        }

        public void Handle(InputReceivedEvent message)
        {
            Interlocked.Increment(ref inputsThisInterval);
        }
    }
}
