﻿using System;
using AuralLog.Infrastructure;
using AuralLog.Messages;
using Midi;

namespace AuralLog
{
    class JazzDrummer : IHandle<MetronomeTickEvent>, IHandle<InputIntensityRatedEvent>
    {
        private readonly IPublishToTopics bus;

        private bool doAFill;
        private int fillBeat;

        public JazzDrummer(IPublishToTopics bus)
        {
            this.bus = bus;
            this.doAFill = false;
            this.fillBeat = -10;
        }

        public void Handle(MetronomeTickEvent message)
        {
            if (message.TicksPerBeat % 2 != 0)
                return; // what to do?! JazzDrummer hates odd time signatures!

            if(fillBeat+2 > message.BeatNumber)
            {
                Percussion drum = Percussion.ChineseCymbal;
                if(fillBeat == message.BeatNumber)
                {
                    if (message.BeatTick * 2 < message.TicksPerBeat)
                        drum = Percussion.HighTom2;
                    else
                        drum = Percussion.MidTom2;
                }
                if(fillBeat+1 == message.BeatNumber)
                {
                    if (message.BeatTick * 2 < message.TicksPerBeat)
                        drum = Percussion.LowTom1;
                    else
                        drum = Percussion.LowTom2;
                }
                bus.Publish(new PercussionOutputEvent
                {
                    Hits = new[] { drum },
                    Velocity = 100,
                    TicksPerBeat = 8
                });
                return;
            }

            if (doAFill && message.BeatTick == message.TicksPerBeat-1)
            {
                fillBeat = message.BeatNumber+1; // next beat!
                doAFill = false;
                return;
            }

            if (message.BeatTick == 0)
            {
                bus.Publish(new PercussionOutputEvent
                {
                    Hits = new[] { Percussion.BassDrum1, Percussion.ClosedHiHat },
                    Velocity = 100,
                    TicksPerBeat = 4
                });
                return;
            }
            if (message.BeatTick * 2 == message.TicksPerBeat)
            {
                bus.Publish(new PercussionOutputEvent
                {
                    Hits = new[] { Percussion.SnareDrum1, Percussion.ClosedHiHat },
                    Velocity = 100,
                    TicksPerBeat = 4
                });
                return;
            }
        }

        public void Handle(InputIntensityRatedEvent message)
        {
            Console.WriteLine(message.InputsPerMetronomeTick);
            if(message.InputsPerMetronomeTick > 3)
                doAFill = true;
        }
    }
}
