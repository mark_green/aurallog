﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace AuralLog
{
    public abstract class ProgramBase
    {
        private int _exitCode;
        private readonly ManualResetEventSlim _exitEvent = new ManualResetEventSlim(false);

        protected abstract void Start();

        public int Run(string[] args)
        {
            try
            {
                Application.RegisterExitAction(Exit);
                Start();
                _exitEvent.Wait();
            }
            catch (Exception ex)
            {
                Application.Exit(ExitCode.Error, FormatExceptionMessage(ex));
            }

            Application.ExitSilent(_exitCode, "Normal exit.");
            return _exitCode;
        }

        private string FormatExceptionMessage(Exception ex)
        {
            string msg = ex.Message;
            var exc = ex.InnerException;
            int cnt = 0;
            while (exc != null)
            {
                cnt += 1;
                msg += "\n" + new string(' ', 2 * cnt) + exc.Message;
                exc = exc.InnerException;
            }
            return msg;
        }

        private void Exit(int exitCode)
        {
            _exitCode = exitCode;
            _exitEvent.Set();
        }
    }

    public enum ExitCode
    {
        Success = 0,
        Error = 1
    }

    public class Application
    {
        private static Action<int> _exit;
        private static int _exited;

        private static readonly HashSet<string> _defines = new HashSet<string>();

        public static void RegisterExitAction(Action<int> exitAction)
        {
            _exit = exitAction;
        }

        public static void ExitSilent(int exitCode, string reason)
        {
            Exit(exitCode, reason, silent: true);
        }

        public static void Exit(ExitCode exitCode, string reason)
        {
            Exit((int)exitCode, reason);
        }

        public static void Exit(int exitCode, string reason)
        {
            Exit(exitCode, reason, silent: false);
        }

        private static void Exit(int exitCode, string reason, bool silent)
        {
            if (Interlocked.CompareExchange(ref _exited, 1, 0) != 0)
                return;


            if (!silent)
            {
                var message = string.Format("Exiting with exit code: {0}.\nExit reason: {1}", exitCode, reason);
                Console.WriteLine(message);
            }

            var exit = _exit;
            if (exit != null)
                exit(exitCode);
        }
    }
}
