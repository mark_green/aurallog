﻿using System;
using System.Threading;
using AuralLog.Infrastructure;
using AuralLog.Messages;

namespace AuralLog
{
    class Metronome : IAmRunnable
    {
        private readonly IPublishToTopics bus;
        private readonly TimeSpan tickInterval;
        private readonly int ticksPerBeat;
        
        private volatile bool keepTicking;
        private Thread thread;

        public Metronome(IPublishToTopics bus, int beatsPerMinute, int ticksPerBeat)
        {
            this.bus = bus;
            this.tickInterval = TimeSpan.FromMilliseconds(1000 / (beatsPerMinute * ticksPerBeat / 60));
            this.ticksPerBeat = ticksPerBeat;
        }

        public void Start()
        {
            thread = new Thread(() => Run());
            keepTicking = true;
            thread.Start();
        }

        public void Stop()
        {
            keepTicking = false;
            thread.Join();
        }

        void Run()
        {
            var tickNumber = 0;
            var beatNumber = 0;
            var beatTick = 0;
            while (keepTicking)
            {
                bus.Publish(new MetronomeTickEvent {
                    BeatNumber = beatNumber,
                    BeatTick = beatTick,

                    TicksPerBeat = ticksPerBeat
                });

                tickNumber++;
                beatTick = tickNumber % ticksPerBeat;
                if (beatTick == 0)
                    beatNumber++;
                Thread.Sleep(tickInterval);
            }
        }

    }
}
