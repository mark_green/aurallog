﻿

namespace AuralLog.Infrastructure
{
    public interface INamedQueue
    {
        string Name { get; }
        int Count();
    }

    public interface IQueuedHandler<in TMessage> :
        INamedQueue,
        IHandle<TMessage> where TMessage : Message
    {
    }
}
