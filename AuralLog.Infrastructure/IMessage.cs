﻿using System;

namespace AuralLog.Infrastructure
{
    public abstract class Message
    {
        public virtual Guid MessageId { get; set; }
        public virtual Guid CausationId { get; set; }
        public virtual string CorrelationId { get; set; }

        protected Message()
        {
            MessageId = Guid.NewGuid();
        }
    }

    public static class MessageExtensions
    {
        public static TMessage Caused<TMessage>(this Message message, Action<TMessage> init = null) where TMessage : Message, new()
        {
            var result = new TMessage
            {
                CausationId = message.MessageId,
                CorrelationId = message.CorrelationId,
            };
            if(init != null)
                init(result);
            return result;
        }

        public static Message Caused(this Message caused, Message result)
        {
            result.CausationId = caused.MessageId;
            result.CorrelationId = caused.CorrelationId;
            return result;
        }
    }
}
