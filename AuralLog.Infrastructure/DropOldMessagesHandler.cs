﻿using System;


namespace AuralLog.Infrastructure
{
    public class DropOldMessagesHandler<TMessage> : IHandle<TMessage> where TMessage : Message
    {
        private readonly IHandle<TMessage> _next;
        public DropOldMessagesHandler(IHandle<TMessage> next)
        {
            _next = next;
        }

        public void Handle(TMessage message)
        {
            var ttlMessage = message as TimedMessage;
            if (ttlMessage != null && ttlMessage.DropDeadDate <= DateTime.Now)
                return;

            _next.Handle(message);
        }
    }
}
