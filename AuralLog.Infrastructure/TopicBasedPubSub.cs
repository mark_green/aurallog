﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;


namespace AuralLog.Infrastructure
{
    public class TopicBasedPubSub : IPublishToTopics, IProvideTopicSubscriptions
    {
        private readonly Dictionary<string, Multiplexer<Message>> _multiplexers = new Dictionary<string, Multiplexer<Message>>();
        private readonly object _multiplexersLock = new object();

        public IEnumerable<string> GetConsumerTypeNamesFor(string topic)
        {
            Multiplexer<Message> multiplexer;
            lock (_multiplexersLock)
            {
                if (_multiplexers.TryGetValue(topic, out multiplexer))
                {
                    return multiplexer.Handlers.Select(x => x.ToString());
                }
            }

            return Enumerable.Empty<string>();
        } 

        public void Publish(string topic, Message message)
        {
            Multiplexer<Message> multiplexer;
            bool topicExists;
            lock (_multiplexersLock)
            {
                topicExists = _multiplexers.TryGetValue(topic, out multiplexer);
            }
            
            if(topicExists)
                multiplexer.Handle(message);
        }

        public void Subscribe<TMessage>(string topic, IHandle<TMessage> handler) where TMessage : Message
        {
            Multiplexer<Message> multiplexer;

            lock (_multiplexersLock)
            {
                if (!_multiplexers.TryGetValue(topic, out multiplexer))
                {
                    multiplexer = new Multiplexer<Message>();
                    _multiplexers.Add(topic, multiplexer);
                }
            }

            multiplexer.AddHandler(handler.NarrowTo<Message, TMessage>());
        }

        public void Unsubscribe<TMessage>(string topic, IHandle<TMessage> handler) where TMessage : Message
        {
            Multiplexer<Message> multiplexer;
            bool topicExists;
            lock (_multiplexersLock)
            {
                topicExists = _multiplexers.TryGetValue(topic, out multiplexer);
            }
            
            if(topicExists)
                multiplexer.RemoveHandler(handler.NarrowTo<Message, TMessage>());
        }
    }
}
