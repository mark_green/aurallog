﻿using System;

namespace AuralLog.Infrastructure
{
    public class NowTimeProvider : IProvideTheTime
    {
        public DateTime GetTheTime()
        {
            return DateTime.Now;
        }
    }
}
