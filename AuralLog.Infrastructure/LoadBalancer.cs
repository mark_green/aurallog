﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace AuralLog.Infrastructure
{
    public class LoadBalancer<TMessage> :
            IAmRunnable,
            IHandle<TMessage>
        where TMessage : Message
    {
        private readonly IPublishToTopics _publisher;
        private readonly IAmRunnable _runner;
        private readonly ThreadedQueuedHandler<TMessage> _next;

        public LoadBalancer(IPublishToTopics publisher, int numberOfHandlers, Func<IHandle<TMessage>> handlerFactory)
        {
            _publisher = publisher;
            
            var handlers = SpawnHandlers(numberOfHandlers, handlerFactory).WithThreadedQueues().ToArray();
            var loadBalancedAndQueuedHandler = handlers.LoadBalanced(3).WithThreadedQueue(string.Format("{0}", typeof(TMessage).Name));

            _runner = Runnable.ComposedOf(
                            loadBalancedAndQueuedHandler,
                            Runnable.ComposedOf(handlers),
                            new QueueMonitor(_publisher, new[]{loadBalancedAndQueuedHandler}, TimeSpan.FromSeconds(1)));

            _next = loadBalancedAndQueuedHandler;
        }

        public void Handle(TMessage message)
        {
            _next.Handle(message);
        }

        private static IEnumerable<IHandle<TMessage>> SpawnHandlers(int numberOfHandlers, Func<IHandle<TMessage>> handlerFactory)
        {
            for (var i = 0; i < numberOfHandlers; i++)
                yield return handlerFactory();
        }

        public void Start()
        {
            _runner.Start();
        }

        public void Stop()
        {
            _runner.Stop();
        }
    }


    public static class LoadBalancer
    {
        public static LoadBalancer<TMessage> ComposedOf<TMessage>(IPublishToTopics publisher, int numberOfHandlers,
                                                        Func<IHandle<TMessage>> handlerFactory)
            where TMessage : Message
        {
            return new LoadBalancer<TMessage>(publisher, numberOfHandlers, handlerFactory);
        }
    }
}
