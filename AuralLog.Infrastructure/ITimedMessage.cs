﻿using System;


namespace AuralLog.Infrastructure
{
    public abstract class TimedMessage : Message
    {
        public DateTime DropDeadDate { get; set; }
    }
}
