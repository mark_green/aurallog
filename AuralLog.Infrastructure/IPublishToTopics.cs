﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace AuralLog.Infrastructure
{
    public interface IPublishToTopics
    {
        void Publish(string topic, Message message);
    }

    public interface IProvideTopicSubscriptions
    {
        void Subscribe<TMessage>(string topic, IHandle<TMessage> consumer) where TMessage : Message;
        void Unsubscribe<TMessage>(string topic, IHandle<TMessage> consumer) where TMessage : Message;
    }

    public static class IPublishToTopicsExtensions
    {
        public static void Publish(this IPublishToTopics publisher, IEnumerable< Message> messages)
        {
            foreach (var message in messages)
                Publish(publisher, message);
        }

        public static void Publish(this IPublishToTopics publisher, Message message)
        {
            DispatchByType(publisher, message);

            if(message.CorrelationId != null)
                publisher.Publish(message.CorrelationId, message);
            publisher.Publish(message.CausationId.ToString(), message);
        }

        public static void SendMeIn(this IPublishToTopics publisher, TimeSpan delay, Message message)
        {
            publisher.Publish(new ScheduledMessage
                {
                    MessageId = Guid.NewGuid(),
                    DueAt = DateTime.Now.Add(delay),
                    Message = message
                });
        }

        private static void DispatchByType(IPublishToTopics publish, Message message)
        {
            var type = message.GetType();
            publish.Publish(type.FullName, message);
            do
            {
                type = type.BaseType;
                publish.Publish(type.FullName, message);
            } while (type != typeof(Message));
        }
    }
}
