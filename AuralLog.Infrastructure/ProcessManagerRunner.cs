﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace AuralLog.Infrastructure
{
    public class ProcessManagerRunner<TStartMessage, TEndMessage> :
        IHandle<Message>
        where TStartMessage : Message
        where TEndMessage : Message
    {
        private readonly Dictionary<string, IProcessManager> _processManagersByCorrelationId;
        private readonly IProcessManagerFactory<TStartMessage> _processManagerFactory;
        private readonly IProvideTopicSubscriptions _subscriptions;

        public ProcessManagerRunner(IProcessManagerFactory<TStartMessage> processManagerFactory, IProvideTopicSubscriptions subscriptions)
        {
            _processManagersByCorrelationId = new Dictionary<string, IProcessManager>();
            _processManagerFactory = processManagerFactory;
            _subscriptions = subscriptions;
        }

        public void Handle(Message message)
        {
            if (message is TStartMessage)
            {
                var startMessage = message as TStartMessage;

                if (string.IsNullOrEmpty(startMessage.CorrelationId))
                {
                    startMessage.CorrelationId = Guid.NewGuid().ToString();
                }

                var processManager = _processManagerFactory.New(startMessage);
                _subscriptions.SubscribeAll(message.CorrelationId, processManager);

                _processManagersByCorrelationId.Add(startMessage.CorrelationId, processManager);

                if (processManager is IHandle<TStartMessage>)
                {
                    ((IHandle<TStartMessage>)processManager).Handle(startMessage);
                }
            }
            else
            {
                var endMessage = message as TEndMessage;
                var processManager = _processManagersByCorrelationId[endMessage.CorrelationId];
                if (processManager is IHandle<TEndMessage>)
                {
                    ((IHandle<TEndMessage>)processManager).Handle(endMessage);
                }
                _processManagersByCorrelationId.Remove(message.CorrelationId);

                _subscriptions.UnsubscribeAll(message.CorrelationId, processManager);
            }
        }
    }

    public class ProcessDoesNotEnd : Message
    {
    }
}
