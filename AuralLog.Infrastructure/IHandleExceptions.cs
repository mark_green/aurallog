﻿using System;


namespace AuralLog.Infrastructure
{
    public interface IHandleExceptions
    {
        void ExceptionOccuredWhileHandling<TMessage>(TMessage message, Exception exception)
            where TMessage : Message;
    }
}
