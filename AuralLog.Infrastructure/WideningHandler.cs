﻿

namespace AuralLog.Infrastructure
{
    public class WideningHandler<TInput, TOutput> : IHandle<TInput>
        where TInput : TOutput
        where TOutput : Message
    {
        private readonly IHandle<TOutput> _next;

        public WideningHandler(IHandle<TOutput> next)
        {
            _next = next;
        }

        public void Handle(TInput message)
        {
            _next.Handle((TOutput)message);
        }

        public override bool Equals(object obj)
        {
            if (obj is WideningHandler<TInput, TOutput>)
            {
                var other = obj as WideningHandler<TInput, TOutput>;
                return _next.Equals(other._next);
            }
            return base.Equals(obj);
        }

        protected bool Equals(WideningHandler<TInput, TOutput> other)
        {
            return Equals(_next, other._next);
        }

        public override int GetHashCode()
        {
            return (_next != null ? _next.GetHashCode() : 0);
        }
    }
}
