﻿using System.Collections.Generic;
using System.Linq;

namespace AuralLog.Infrastructure
{
    public class LoadBalancingDispatchingHandler<TMessage> : IHandle<TMessage> where TMessage : Message
    {
        private readonly IQueuedHandler<TMessage>[] _workers;
        private readonly int _maxWorkerQueueSize;

        public LoadBalancingDispatchingHandler(IEnumerable<IQueuedHandler<TMessage>> workers, int maxWorkerQueueSize)
        {
            _workers = workers.ToArray();
            _maxWorkerQueueSize = maxWorkerQueueSize;
        }

        public void Handle(TMessage message)
        {
            IQueuedHandler<TMessage> chosenWorker;
            do
            {
                chosenWorker = _workers[0];
                for (var i = 1; i < _workers.Count(); i++)
                    if (_workers[i].Count() < chosenWorker.Count())
                        chosenWorker = _workers[i];
            } while (chosenWorker.Count() > _maxWorkerQueueSize);
            chosenWorker.Handle(message);
        }
    }
}
