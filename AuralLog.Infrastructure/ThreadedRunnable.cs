﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AuralLog.Infrastructure
{
    public abstract class ThreadedRunnable : IAmRunnable
    {
        private Thread thread;
        private volatile bool running;

        protected abstract void WhileRunning();

        private void Run()
        {
            while (running)
                WhileRunning();
        }

        public void Start()
        {
            thread = new Thread(() => Run());
            running = true;
            thread.Start();
        }

        public void Stop()
        {
            running = false;
            thread.Join();
        }
    }
}
