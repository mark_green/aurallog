﻿using System.Linq;

namespace AuralLog.Infrastructure
{
    public static class ProvideTopicSubscriptionsExtensions
    {
        public static void Subscribe<TMessage>(this IProvideTopicSubscriptions subscriptionProvider, IHandle<TMessage> handler)
            where TMessage : Message
        {
            subscriptionProvider.Subscribe(typeof(TMessage).FullName, handler);
        }

        public static void SubscribeAll(this IProvideTopicSubscriptions bus, string topic, object handler)
        {
            Invoke(bus, topic, handler, "Subscribe");
        }

        public static void UnsubscribeAll(this IProvideTopicSubscriptions bus, string topic, object handler)
        {
            Invoke(bus, topic, handler, "Unsubscribe");
        }

        private static void Invoke(IProvideTopicSubscriptions bus, string topic, object handler, string methodName)
        {
            var methodInfo = bus.GetType().GetMethod(methodName);

            foreach (var handlerType in handler.GetType()
                .GetInterfaces()
                .Where(x => x.Name.Contains("IHandle")))
            {
                var messageType = handlerType.GetGenericArguments().First();
                var method = methodInfo.MakeGenericMethod(messageType);
                method.Invoke(bus, new object[] { topic, handler });
            }
        }
    }
}