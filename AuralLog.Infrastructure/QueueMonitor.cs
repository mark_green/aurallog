﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AuralLog.Infrastructure
{
    public class QueueMonitor : IAmRunnable
    {
        private readonly IPublishToTopics _publisher;
        private readonly INamedQueue[] _queuesToMonitor;
        private readonly TimeSpan _monitoringInterval;

        private readonly TimeSpan _threadStopWaitTimeout = TimeSpan.FromSeconds(10);
        private readonly ManualResetEventSlim _stopped = new ManualResetEventSlim(true);
        private Thread _thread;
        private volatile bool _stop;

        public QueueMonitor(IPublishToTopics publisher, IEnumerable<INamedQueue> queuesToMonitor, TimeSpan monitoringInterval)
        {
            _publisher = publisher;
            _queuesToMonitor = queuesToMonitor.ToArray();
            _monitoringInterval = monitoringInterval;
        }

        private void MonitorQueues()
        {
            while (!_stop || _queuesToMonitor.Any(q => q.Count() > 0))
            {
                Thread.Sleep(_monitoringInterval);
                foreach(var queue in _queuesToMonitor)
                    _publisher.Publish(new QueueStatusEvent
                        {
                            QueueName = queue.Name,
                            QueueSize = queue.Count(),

                            MessageId = Guid.NewGuid()
                        });
            }
            _stopped.Set(); //signal that the thread has completed.
        }

        public void Start()
        {
            _stopped.Reset();

            _thread = new Thread(MonitorQueues) { IsBackground = true, Name = GetType().Name };
            _thread.Start();
        }

        public void Stop()
        {
            _stop = true;
            while (_queuesToMonitor.Any(q => q.Count() > 0)) // wait for queues to drain first
                Thread.Sleep(10);
            if (!_stopped.Wait(_threadStopWaitTimeout))// wait for thread to signal completion.
                throw new TimeoutException(string.Format("Unable to stop thread"));
        }
    }
}
