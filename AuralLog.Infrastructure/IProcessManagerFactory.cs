﻿namespace AuralLog.Infrastructure
{
    public interface IProcessManagerFactory<in TStartMessage> where TStartMessage : Message
    {
        IProcessManager New(TStartMessage startMessage);
    }
}
