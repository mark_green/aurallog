﻿namespace AuralLog.Infrastructure
{
    public interface IAmRunnable
    {
        void Start();
        void Stop();
    }
}
