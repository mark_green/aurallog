﻿using System.Threading;

namespace AuralLog.Infrastructure
{
    public class AlarmClock : IHandle<ScheduledMessage>
    {
        private readonly IProvideTheTime _timeProvider;
        private readonly IPublishToTopics _publisher;

        public AlarmClock(
            IProvideTheTime timeProvider,
            IPublishToTopics publisher)
        {
            _timeProvider = timeProvider;
            _publisher = publisher;
        }

        public void Handle(ScheduledMessage message)
        {
            if (_timeProvider.GetTheTime() < message.DueAt)
            {
                Thread.Sleep(1);
                _publisher.Publish(message);
            }
            else
                _publisher.Publish(message.Message);
        }
    }
}
