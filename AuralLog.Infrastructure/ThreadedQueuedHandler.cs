﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace AuralLog.Infrastructure
{
    public class ThreadedQueuedHandler<TMessage> :
        IAmRunnable,
        IDisposable,
        IQueuedHandler<TMessage> where TMessage : Message
    {
        private readonly ConcurrentQueue<TMessage> _queue = new ConcurrentQueue<TMessage>();
        private readonly IHandle<TMessage> _handler;

        private readonly TimeSpan _threadStopWaitTimeout = TimeSpan.FromSeconds(10);
        private readonly ManualResetEventSlim _stopped = new ManualResetEventSlim(true);
        private Thread _thread;
        private volatile bool _stop;

        public string Name { get; private set; }

        public ThreadedQueuedHandler(IHandle<TMessage> handler, string name)
        {
            Name = name;
            _handler = handler;
        }

        private void ReadFromQueue()
        {
            while (!_stop || _queue.Count > 0)
            {
                TMessage message;
                if (_queue.TryDequeue(out message))
                    _handler.Handle(message);
                else
                    Thread.Sleep(1);
            }
            _stopped.Set(); //signal that the thread has completed.
        }

        public void Handle(TMessage message)
        {
            _queue.Enqueue(message);
        }

        public int Count()
        {
            return _queue.Count();
        }

        public void Start()
        {
            _stopped.Reset();

            _thread = new Thread(ReadFromQueue) { IsBackground = true, Name = Name };
            _thread.Start();
        }

        public void Stop()
        {
            _stop = true;
            while (_queue.Count > 0) // wait for queue to drain first
                Thread.Sleep(10);
            if (!_stopped.Wait(_threadStopWaitTimeout))// wait for thread to signal completion.
                throw new TimeoutException(string.Format("Unable to stop thread {0}", Name));
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
