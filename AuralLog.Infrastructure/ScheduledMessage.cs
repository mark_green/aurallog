﻿using System;


namespace AuralLog.Infrastructure
{
    public class ScheduledMessage : Message
    {
        public Message Message { get; set; }
        public DateTime DueAt { get; set; }

        public override Guid MessageId { get; set; }
        public override string CorrelationId
        {
            get { return Message.CorrelationId; }
            set { Message.CorrelationId = value; }
        }
        public override Guid CausationId
        {
            get { return Message.CausationId; }
            set { Message.CausationId = value; }
        }
    }

    public static class Scheduling
    {
        public static ScheduledMessage ScheduledMessage(Message message, TimeSpan delay)
        {
            return new ScheduledMessage
            {
                Message = message,
                DueAt = DateTime.Now.Add(delay)
            };
        }
        public static ScheduledMessage ScheduledMessage(Message message, DateTime dueAt)
        {
            return new ScheduledMessage
            {
                Message = message,
                DueAt = dueAt
            };
        }
    }
}
