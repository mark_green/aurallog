﻿using System.Collections.Generic;
using System.Linq;


namespace AuralLog.Infrastructure
{
    public class Multiplexer<TMessage> : IHandle<TMessage> where TMessage : Message
    {
        private readonly List<IHandle<TMessage>> _handlers = new List<IHandle<TMessage>>();

        public IEnumerable<IHandle<TMessage>> Handlers  {  get { return _handlers; } }

        public void AddHandler(IHandle<TMessage> handler)
        {
            _handlers.Add(handler);
        }

        public void Handle(TMessage message)
        {
            for (var i = 0; i < _handlers.Count; i++)
                _handlers[i].Handle(message);
        }

        internal void RemoveHandler(IHandle<TMessage> handler)
        {
            var existing = _handlers.FirstOrDefault(h => h.Equals(handler));
            if (existing != null)
                _handlers.Remove(existing);
        }
    }
}
