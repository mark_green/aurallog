﻿using System;

namespace AuralLog.Infrastructure
{
    public class QueueStatusEvent : Message
    {
        public string QueueName { get; set; }
        public int QueueSize { get; set; }
    }
}
