﻿using System.Collections.Generic;
using System.Linq;


namespace AuralLog.Infrastructure
{
    public class RoundRobinDispatchingHandler<TMessage> : IHandle<TMessage> where TMessage : Message
    {
        private int _next;

        private readonly IHandle<TMessage>[] _workers;
        public RoundRobinDispatchingHandler(IEnumerable<IHandle<TMessage>> workers)
        {
            _workers = workers.ToArray();
            _next = -1;
        }

        public void Handle(TMessage message)
        {
            _next = (_next+1) % _workers.Length;
            _workers[_next].Handle(message);
        }
    }
}
