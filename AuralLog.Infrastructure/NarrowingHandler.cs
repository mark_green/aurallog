﻿

namespace AuralLog.Infrastructure
{
    public class NarrowingHandler<TInput, TOutput> : IHandle<TInput>
        where TInput : Message 
        where TOutput : class, TInput 
    {
        private readonly IHandle<TOutput> _next;

        public NarrowingHandler(IHandle<TOutput> next)
        {
            _next = next;
        }
        public void Handle(TInput message)
        {
            var narrowedMessage = message as TOutput;
            if (narrowedMessage == null)
                return;
            
            _next.Handle(narrowedMessage);
        }

        public override bool Equals(object obj)
        {
            if (obj is NarrowingHandler<TInput, TOutput>)
            {
                var other = obj as NarrowingHandler<TInput, TOutput>;
                return _next.Equals(other._next);
            }
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return _next.ToString();
        }

        protected bool Equals(NarrowingHandler<TInput, TOutput> other)
        {
            return Equals(_next, other._next);
        }

        public override int GetHashCode()
        {
            return (_next != null ? _next.GetHashCode() : 0);
        }
    }
}
