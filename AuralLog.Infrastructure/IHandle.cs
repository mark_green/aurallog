﻿using System;
using System.Collections.Generic;


namespace AuralLog.Infrastructure
{
    public interface IHandle<in TMessage> where TMessage : Message
    {
        void Handle(TMessage message);
    }

    public static class Handlers
    {
        public static IEnumerable<IHandle<TMessage>> Create<TMessage>(int handlers, Func<IHandle<TMessage>> factory)
             where TMessage : Message
        {
            for (var i = 0; i < handlers; i++)
                yield return factory();
        }
    }
}
